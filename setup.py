#!/usr/bin/env python
"""
Install wagpress_news using setuptools
"""
from setuptools import find_packages, setup

with open('wagpress_news/version.py') as f:
    version = None
    exec(f.read())

with open('README.rst') as f:
    readme = f.read()

# Documentation dependencies
documentation_extras = [
    'Sphinx>=1.4.6',
    'sphinx-autobuild>=0.5.2',
    'sphinx_rtd_theme>=0.1.8',
    'sphinxcontrib-spelling==2.1.1',
    'pyenchant==1.6.6',
]

setup(
    name='wagpress_news',
    version=version,
    description='News plugin for the Wagtail CMS',
    long_description=readme,
    author='Scott Sharkey <ssharkey@lanshark.com>',
    author_email='ssharkey@lanshark.com',
    url='https://gitlab.com/lansharkconsulting/wagpress/wagpress_news/',

    install_requires=[
        'wagtail>=2.15.0',
    ],
    extras_require={
        'docs': documentation_extras
    },
    zip_safe=False,
    license='BSD License',

    packages=find_packages(exclude=['tests*']),

    include_package_data=True,
    package_data={},

    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Framework :: Django',
        'License :: OSI Approved :: BSD License',
    ],
)
