from django.dispatch import Signal

newsitem_published = Signal()
newsitem_unpublished = Signal()
newsitem_draft_saved = Signal()
newsitem_deleted = Signal()
