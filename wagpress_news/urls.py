from django.urls import path, re_path

from .views import chooser, editor

app_name = 'wagpress_news'
urlpatterns = [
    path('', chooser.choose, name='choose'),
    path('search/', chooser.search, name='search'),
    path('<int:pk>/', chooser.index, name='index'),
    path('<int:pk>/create/', editor.create, name='create'),
    re_path(r'^(?P<pk>\d+)/edit/(?P<newsitem_pk>.*)/$', editor.edit, name='edit'),
    re_path(r'^(?P<pk>\d+)/unpublish/(?P<newsitem_pk>.*)/$', editor.unpublish, name='unpublish'),
    re_path(r'^(?P<pk>\d+)/delete/(?P<newsitem_pk>.*)/$', editor.delete, name='delete'),
    re_path(r'^(?P<pk>\d+)/view_draft/(?P<newsitem_pk>.*)/$', editor.view_draft, name='view_draft'),
    # Choosers
    path('chooser/', chooser.choose_modal, name='chooser'),
    path('chooser/<int:pk>/<int:newsitem_pk>/', chooser.chosen_modal, name='chosen'),
]
