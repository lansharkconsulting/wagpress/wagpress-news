.. _setup:

=====
Setup
=====

``wagpress_news`` is compatible with Wagtail 2.3 and higher,
Django 1.11 and higher,
and Python 3.4 and higher.

For older version of Wagtail or Django, see past releases

Step 1
______

Install ``wagpress_news`` using pip::

   pip install wagpress_news

Step 2
______

Add ``wagpress_news`` and ``wagtail.contrib.routable_page`` to your ``INSTALLED_APPS`` in settings:

.. code-block:: python

  INSTALLED_APPS += [
      'wagpress_news',
      'wagtail.contrib.routable_page',
  ]
