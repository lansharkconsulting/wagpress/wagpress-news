.. _forms:

==========================
Forms, widgets, and blocks
==========================

Wagtail news integrates with Wagtail edit handlers and stream field blocks.


.. module:: wagpress_news.edit_handlers

``NewsChooserPanel``
====================

.. autoclass:: NewsChooserPanel

.. module:: wagpress_news.blocks

``NewsChooserBlock``
====================

.. autoclass:: NewsChooserBlock
