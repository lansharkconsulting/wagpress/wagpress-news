import datetime
from urllib.parse import quote as urlquote

from django.test import TestCase
from django.utils import timezone
from wagtail.core.models import Site
from wagtail.tests.utils import WagtailTestUtils

from tests.app.models import NewsIndex, NewsItem


class TestNewsItem(TestCase, WagtailTestUtils):

    def setUp(self):
        super().setUp()
        site = Site.objects.get(is_default_site=True)
        root_page = site.root_page
        self.index = NewsIndex(
            title='News', slug='news')
        root_page.add_child(instance=self.index)
        ni_date = timezone.make_aware(datetime.datetime(2017, 4, 13, 12, 0, 0))
        self.newsitem = NewsItem.objects.create(
            newsindex=self.index,
            title='A post',
            date=ni_date)

    def test_view(self):
        response = self.client.get(self.newsitem.url())

        # Check the right NewsIndex was used, and is its most specific type
        self.assertIsInstance(response.context['self'], NewsIndex)
        self.assertEqual(response.context['self'], self.index)
        self.assertEqual(response.context['page'], self.index)

        # Check the right NewsItem was used
        self.assertEqual(response.context['newsitem'], self.newsitem)

        # Check the NewsIndex context is used as a base
        self.assertEqual(response.context['extra'], 'foo')

        # Check the context can be overridden using NewsItem.get_context()
        self.assertEqual(response.context['foo'], 'bar')

    def test_bad_url_redirect(self):
        response = self.client.get(
            f'/news/1234/2/3/{self.newsitem.pk}-bad-title/',
            follow=True)

        self.assertEqual(
            self.newsitem.url(),
            urlquote(f'/news/2017/4/13/{self.newsitem.pk}-a-post/'))
        self.assertEqual(
            response.redirect_chain,
            [(self.newsitem.url(), 301)])

    def test_bad_url_redirect_unicode(self):
        self.newsitem.title = '你好，世界！'
        self.newsitem.save()

        response = self.client.get(
            f'/news/1234/2/3/{self.newsitem.pk}-bad-title/',
            follow=True)

        self.assertEqual(
            self.newsitem.url(),
            urlquote(f'/news/2017/4/13/{self.newsitem.pk}-你好世界/'))
        self.assertEqual(
            response.redirect_chain,
            [(self.newsitem.url(), 301)])
